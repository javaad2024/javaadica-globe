//
//  Javaadica_GlobeApp.swift
//  Javaadica Globe
//
//  Created by AdvocatesClose on 3/14/24.
//

import SwiftUI

@main
struct Javaadica_GlobeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
